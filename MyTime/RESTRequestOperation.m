//
//  RestKitRequestOperationManager.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "RESTRequestOperation.h"

@interface RESTRequestOperation()
@property (nonatomic) NSIndexSet *statusCodesSuccessful;
@end

@implementation RESTRequestOperation

#pragma mark - Singleton Init
+(id)instance
{
    static RESTRequestOperation *manager = nil;
    @synchronized(self)
    {
        if (!manager)
            manager = [[self alloc]init];
    }
    
    return manager;
}

-(id)init
{
    if (self = [super init])
    {
        self.statusCodesSuccessful = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful);
    }
    
    return self;
}

#pragma mark - Request Operations

#pragma mark - Request Operations With Mapping

-(RKResponseDescriptor*)getDescriptorWithClientErrorCodes
{
    NSIndexSet *errorCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[[RESTObjectMapper instance]mappingForErrors] method:RKRequestMethodAny pathPattern:nil keyPath:@"errors" statusCodes:errorCodes];
    
    return errorDescriptor;
}

-(RKResponseDescriptor*)getDescriptorWithServerErrorCodes
{
    NSIndexSet *errorCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassServerError);
    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[[RESTObjectMapper instance]mappingForErrors] method:RKRequestMethodAny pathPattern:nil keyPath:@"errors" statusCodes:errorCodes];
    
    return errorDescriptor;
}

-(RKObjectRequestOperation*)requestOperationWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping requestMethod:(RKRequestMethod)method
{
    NSAssert(objectMapping, @"Object mapping cannot be nil");
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:objectMapping method:RKRequestMethodAny pathPattern:nil keyPath:nil statusCodes:self.statusCodesSuccessful];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    RKObjectRequestOperation *requestOperation = [[RKObjectRequestOperation alloc] initWithRequest:request responseDescriptors:@[responseDescriptor,[self getDescriptorWithClientErrorCodes],[self getDescriptorWithServerErrorCodes]]];
    
    return requestOperation;
}


//RAHUL: Note that I added PUT and POST to show how this code might be
//properly extended in the future

-(RKObjectRequestOperation*)requestOperationPUTWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping
{
    return [self requestOperationWithURL:url mapping:objectMapping requestMethod:RKRequestMethodPUT];
}

-(RKObjectRequestOperation*)requestOperationPOSTWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping
{
    return [self requestOperationWithURL:url mapping:objectMapping requestMethod:RKRequestMethodPOST];
}

-(RKObjectRequestOperation*)requestOperationGETWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping
{
    return [self requestOperationWithURL:url mapping:objectMapping requestMethod:RKRequestMethodGET];
}



@end
