//
//  DataOperationDelegate.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

/********************************************
 This protocol defines methods that are used
 by controllers in response to some type of
 data operation.
 *******************************************/

#import <Foundation/Foundation.h>
@class Business;

@protocol DataOperationDelegate <NSObject>
@optional
/*!
 * This delegate method provides a list of Business objects
 */
-(void)onSearchResults:(NSArray*)businesses;
-(void)onFailure:(NSString*)failureType;
@end


//RAHUL: This protocol highly decouples relationships between network
//operations and the end result. So if we ever moved away from REST
//network operations to something else, this protocol (and the controllers)
//that implement it shouldn't have to change.