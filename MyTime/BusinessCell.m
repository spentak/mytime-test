//
//  BusinessCell.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//
#define ROW_HEIGHT 117.0
#import "BusinessCell.h"
#import "Business.h"
#import "NSDateFormatter+FormatDate.h"
#import "ImageCache.h"

@implementation BusinessCell


+(CGFloat)getRowHeight
{
    return ROW_HEIGHT;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.companyName.font = [DesignManager getHeaderFontWithSize:14.0];
    self.companyType.font = [DesignManager getSubHeaderFontWithSize:12.0];
    
    self.companyName.textColor = [DesignManager getHeadingTextColor];
    self.companyType.textColor = [DesignManager getSubHeadingTextColor];
    
    self.distance.font = [DesignManager getSubHeaderFontWithSize:12.0];
    self.nextAppt.font = [DesignManager getSubHeaderFontWithSize:12.0];
    
    self.distance.textColor = [DesignManager getSubHeadingTextColor];
    self.nextAppt.textColor = [DesignManager getMyTimeTeal];
    
    self.price.font = [DesignManager getHeaderFontWithSize:15.0];
    self.price.textColor = [DesignManager getHeadingTextColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)hideApptControls:(BOOL)hidden
{
    self.nextAppt.hidden = hidden;
    self.boltImg.hidden = hidden;
}

-(void)hidePricingControls:(BOOL)hidden
{
    self.price.hidden = hidden;
    self.saleTag.hidden = hidden;
}

-(void)hideYelpControls:(BOOL)hidden
{
    self.yelpStars.hidden = hidden;
    self.yelpLogo.hidden = hidden;
}

-(void)refreshCellWithBusiness:(Business*)business
{
    _business = business;
    [self.companyName setText:self.business.name];
    [self.companyType setText:self.business.service_name];
    
    if (business.next_appointment_times)
    {
        if (business.next_appointment_times.count >= 1)
        {
            [self hideApptControls:NO];
            NSString *dateStr = [NSDateFormatter getFormattedDateStringRFC3339:[business.next_appointment_times objectAtIndex:0]];
            [self.nextAppt setText:[NSString stringWithFormat:@"Next appt %@",dateStr]];
        }
        else
        {
            [self hideApptControls:YES];
        }
    }
    else
    {
        [self hideApptControls:YES];
    }
    
    //In production app I would also be moving the "sale" image to the left or right based on the length of the price label
    //(right now that sale image is in a static position). But I'm running short on time!
    if (business.min_price && business.max_price)
    {
        [self hidePricingControls:NO];
        NSString *string = [NSString stringWithFormat:@"$%@ - $%@",[business.min_price stringValue],[business.max_price stringValue]];
        [self.price setText:string];
    }
    else
    {
        [self hidePricingControls:YES];
    }
    
    if (business.yelp_rating_image_url)
    {
        [self hideYelpControls:NO];
        [[ImageCache sharedInstance]downloadImageAtURL:[NSURL URLWithString:business.yelp_rating_image_url] completionHandler:^(UIImage *image) {
            [self.yelpStars setImage:image];
            [self setNeedsLayout];
        }];
    }
    else
    {
        [self hideYelpControls:YES];
    }
    
    if (business.photo_url)
    {
        [[ImageCache sharedInstance]downloadImageAtURL:[NSURL URLWithString:business.photo_url] completionHandler:^(UIImage *image) {
            [self.profilePic setImage:image];
            [self setNeedsLayout];
        }];
    }
    
    
    
    
}



@end
