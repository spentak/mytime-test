//
//  UIView+ViewBlur.h
//  jillsoffice
//
//  Created by Mark Price on 8/11/14.
//  Copyright (c) 2014 Verisage. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ViewFX)
+(void)roundCorners:(UIView*)view cornerRadius:(CGFloat)cornerRadius;
+(void)colorBorder:(UIView*)view borderWidth:(CGFloat)width borderColor:(CGColorRef)color;

@end
