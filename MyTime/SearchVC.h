//
//  ViewController.h
//  MyTime
//
//  Created by Mark Price on 9/16/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataOperationDelegate.h"

@interface SearchVC : UIViewController <DataOperationDelegate>

@end
