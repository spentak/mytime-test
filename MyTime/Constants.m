//
//  Constants.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "Constants.h"

//URLS
NSString *const URL_BASE = @"http://www.mytime.com/api/";
NSString *const URL_API_VERSION = @"v1/";

//Normally this would be generated dynamically via the app and a search so we break
//this apart here for sake of setting up proper urls
NSString *const URL_MOCK_SEARCH = @"deals.json?what=Massage&when=Anytime&where=34.052200,-118.242800";

//FAILURE TYPES
//At some point in the future I would probably make these an enum to make switch
//evaluations rather than if-then checks
NSString *const FAILURE_TYPE_NO_INTERNET = @"failure_type_no_internet";
NSString *const FAILURE_TYPE_NO_SEARCH_RESULTS = @"failure_type_no_search_results";
NSString *const FAILURE_TYPE_GENERIC = @"failure_type_generic";