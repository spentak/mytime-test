//
//  ViewController.m
//  MyTime
//
//  Created by Mark Price on 9/16/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "SearchVC.h"
#import "RESTDataOperation.h"
#import "BusinessCell.h"

@interface SearchVC () <UITableViewDataSource,UITableViewDelegate>

//IBOutlets
@property (nonatomic,weak) IBOutlet UITableView *resultsTable;
@property (nonatomic,weak) IBOutlet UIButton *filterBtn;
@property (nonatomic,weak) IBOutlet UIButton *mapBtn;
@property (nonatomic,weak) IBOutlet UILabel *searchLbl;

//Data
@property (nonatomic) NSArray *businesses;

@end

@implementation SearchVC

#pragma mark - UIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupUI];
    self.resultsTable.delegate = self;
    self.resultsTable.dataSource = self;
    [self search];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SearchVC
-(void)onSearchResults:(NSArray *)businesses
{
    self.businesses = businesses;
    [self.resultsTable reloadData];
}

-(void)search
{
    if ([[RESTManager instance] internetActive])
    {
        RESTDataOperation *operation = [[RESTDataOperation alloc]init];
        operation.delegate = self;
        [operation search:@""];
    }
    else
    {
        [self showBasicAlertWithTitle:@"No Internet" description:@"Cannot detect Internet connection. Please reconnect"];
    }
}

-(void)onFailure:(NSString *)failureType
{
    if (failureType == FAILURE_TYPE_NO_SEARCH_RESULTS)
    {
        [self showBasicAlertWithTitle:@"No Results" description:@"Could not find any results for your search."];
    }
    else if (failureType == FAILURE_TYPE_GENERIC)
    {
        [self showBasicAlertWithTitle:@"Could Not Search" description:@"There was a problem finding results. Please try again later."];
    }
}

-(void)showBasicAlertWithTitle:(NSString*)title description:(NSString*)desc
{
    //RAHUL: Normally these strings would be localized
    //We might also have the master view controller handle
    //Alerts (But we are using this VC as the master VC because
    //it is a single view app
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:desc delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellID = @"CELLID";
    BusinessCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (!cell)
    {
        [tableView registerNib:[UINib nibWithNibName:@"BusinessCell" bundle:nil] forCellReuseIdentifier:cellID];
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    }
    
    [DesignManager roundTableViewCellCorners:cell.controlHolder];
    
    [cell refreshCellWithBusiness:[self.businesses objectAtIndex:indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.businesses.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [BusinessCell getRowHeight];
}

#pragma mark - UI
-(void)setupUI
{
    [DesignManager createButtonBorder:self.filterBtn];
    [DesignManager createButtonBorder:self.mapBtn];
    self.searchLbl.font = [DesignManager getSubHeader2FontWithSize:18.0];
    self.searchLbl.textColor = [DesignManager getSubHeadingTextColor];
    
}


@end
