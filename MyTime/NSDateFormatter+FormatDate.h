//
//  NSDateFormatter+FormatDate.h
//  jillsoffice
//
//  Created by Mark Price on 8/11/14.
//  Copyright (c) 2014 Verisage. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (FormatDate)
+(NSString*)getStringFromDate:(NSDate*)date;
+(NSDate*)getDateFromString:(NSString*)stringDate;
+(NSString*)getFormattedDateStringRFC3339:(NSString*)rawDate;
@end
