//
//  Business.h
//  MyTime
//
//  Created by Mark Price on 9/16/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"

@interface Business : NSObject
@property (nonatomic) NSString *photo_url;
@property (nonatomic) NSString *name;
@property (nonatomic) Location *location;
@property (nonatomic) NSNumber *min_price;
@property (nonatomic) NSNumber *max_price;
@property (nonatomic) BOOL on_sale;
@property (nonatomic) NSString *service_name;
@property (nonatomic) NSArray *next_appointment_times;
@property (nonatomic) NSString *yelp_rating_image_url;
@end
