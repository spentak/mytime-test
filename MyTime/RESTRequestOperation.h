//
//  RestKitRequestOperationManager.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

/*********************************
 This singelton class is used to 
 create request operations for rest
 kit. For the various network calls
 *********************************/

#import <Foundation/Foundation.h>

@interface RESTRequestOperation : NSObject
+(id)instance;

/*!
 * This method is for GET request operations that expect to receive
 * response data via object mapping
 * \param url The URL to be used
 * \param mapping the object mapping to use
 * \returns RKObjectRequestOperation to be used in network call
 */
-(RKObjectRequestOperation*)requestOperationGETWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping;

/*!
 * This method is for POST request operations that expect to post
 * data via object mapping
 * \param url The URL to be used
 * \param mapping the object mapping to use
 * \returns RKObjectRequestOperation to be used in network call
 */
-(RKObjectRequestOperation*)requestOperationPOSTWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping;

/*!
 * This method is for PUT request operations that expect to modify
 * data via object mapping
 * \param url The URL to be used
 * \param mapping the object mapping to use
 * \returns RKObjectRequestOperation to be used in network call
 */
-(RKObjectRequestOperation*)requestOperationPUTWithURL:(NSURL*)url mapping:(RKObjectMapping*)objectMapping;
@end
