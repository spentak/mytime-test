//
//  Location.h
//  MyTime
//
//  Created by Mark Price on 9/16/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject
@property (nonatomic) NSString *latitude;
@property (nonatomic) NSString *longitude;
@end
