//
//  BusinessCell.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Business;
@interface BusinessCell : UITableViewCell
@property (nonatomic,weak) IBOutlet UILabel *companyName;
@property (nonatomic,weak) IBOutlet UILabel *companyType;
@property (nonatomic,weak) IBOutlet UILabel *distance;
@property (nonatomic,weak) IBOutlet UILabel *nextAppt;
@property (nonatomic,weak) IBOutlet UIImageView *boltImg;
@property (nonatomic,weak) IBOutlet UILabel *price;
@property (nonatomic,weak) IBOutlet UIImageView *yelpStars;
@property (nonatomic,weak) IBOutlet UIImageView *yelpLogo;
@property (nonatomic,weak) IBOutlet UIImageView *saleTag;
@property (nonatomic,weak) IBOutlet UIView *controlHolder;
@property (nonatomic,weak) IBOutlet UIImageView *profilePic;
@property (nonatomic,readonly) Business *business;

-(void)refreshCellWithBusiness:(Business*)business;
+(CGFloat)getRowHeight;
@end
