//
//  DataOperation.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "RESTDataOperation.h"
#import "Business.h"
#import "Location.h"
@implementation RESTDataOperation

//RAHUL: This block-based API call seemed the most appropriate since we are only making one network call
//In fact even with a more complex system where we hit many endpoints I might still use the same model
//the important thing is that we keep the controller decoupled (hence the delegate method call)
//the other option is have delegate methods here that receive success and response calls from the request
//which would probably be a similar amount of work and reliability as blocks
-(void)search:(NSString*)searchText
{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",URL_BASE,URL_API_VERSION,URL_MOCK_SEARCH];
    NSURL *url = [NSURL URLWithString:urlString];
    
    NSLog(@"URL: %@",urlString);
    RKObjectRequestOperation *request = [[RESTRequestOperation instance]requestOperationGETWithURL:url mapping:[[RESTObjectMapper instance]mappingForBusiness]];
    
    //RAHUL: RestKit and AFNetworking nicely automatically handle sending requests on async background thread
    //then returning to the main thread (hence why there is no async_dispatch here)
    [request setCompletionBlockWithSuccess:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
    {
        //No search results
        if (mappingResult.count == 0)
        {
            if ([self.delegate respondsToSelector:@selector(onFailure:)])
                [self.delegate onFailure:FAILURE_TYPE_NO_SEARCH_RESULTS];
        }
        else
        {
            if ([self.delegate respondsToSelector:@selector(onSearchResults:)])
            {
                [self.delegate onSearchResults:mappingResult.array];
            }
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error)
    {
        if (error)
        {
            if ([[RESTManager instance]isClientError:[error code]])
            {
                //If the code is written correctly we should never really see this at a user level - if this
                //was authentication we would respond accordingly
                
                if ([self.delegate respondsToSelector:@selector(onFailure:)])
                    [self.delegate onFailure:FAILURE_TYPE_GENERIC];
            }
            else if ([[RESTManager instance]isServerError:[error code]])
            {
                //This might indicate a server failure - could handle this specifically (like re-trying the request one or more times)
                //but for now we will show a general failure
                if ([self.delegate respondsToSelector:@selector(onFailure:)])
                    [self.delegate onFailure:FAILURE_TYPE_GENERIC];
            }
        }
    }];
    
    [request start];
}


@end
