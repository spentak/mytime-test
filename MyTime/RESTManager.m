//
//  RESTManager.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "RESTManager.h"
#import "Reachability.h"
@implementation RESTManager

+(id)instance
{
    static RESTManager *manager = nil;
    
    @synchronized(self)
    {
        if (!manager)
            manager = [[self alloc]init];
    }
    
    return manager;
}

-(id)init
{
    if (self = [super init])
    {
        _internetActive = YES;
        
        // Allocate a reachability object
        Reachability* reach = [Reachability reachabilityWithHostname:@"www.google.com"];
        
        // Set the blocks
        reach.reachableBlock = ^(Reachability*reach)
        {
            // keep in mind this is called on a background thread
            // and if you are updating the UI it needs to happen
            // on the main thread, like this:
            
            dispatch_async(dispatch_get_main_queue(), ^{
                _internetActive = YES;
                NSLog(@"INTERNET REACHABLE!");
            });
        };
        
        reach.unreachableBlock = ^(Reachability*reach)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                _internetActive = NO;
            });
            NSLog(@"INTERNET UNREACHABLE!");
        };
        
        // Start the notifier, which will cause the reachability object to retain itself!
        [reach startNotifier];
        
        _objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:URL_BASE]];

    }
    
    return self;

}

-(BOOL)isClientError:(NSInteger)errorCode
{
    if (errorCode >= 400 && errorCode < 500)
        return YES;
    
    return NO;
}

-(BOOL)isServerError:(NSInteger)errorCode
{
    if (errorCode >= 500 && errorCode < 600)
        return YES;
    
    return NO;
}




@end
