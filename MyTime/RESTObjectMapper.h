//
//  RKObjectMapper.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

/********************************************
 This class is specific for object mapping
 via Rest Kit. Will likely frequently change
 over the course of a project as APIs change
 Class is decoupled from other classes to 
 prevent errors and the need to make changes
 application wide. Rest Kit uses KVC to make
 managing data from the network simple.
 *******************************************/


#import <Foundation/Foundation.h>



@interface RESTObjectMapper : NSObject
+(id)instance;
-(RKObjectMapping*)mappingForBusiness;
-(RKObjectMapping*)mappingForLocation;
-(RKObjectMapping*)mappingForErrors;
@end
