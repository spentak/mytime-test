//
//  RKObjectMapper.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "RESTObjectMapper.h"
#import "Business.h"
#import "Location.h"

@implementation RESTObjectMapper

+(id)instance
{
    static RESTObjectMapper *mapper = nil;
    
    @synchronized(self)
    {
        if (!mapper)
            mapper = [[self alloc]init];
    }
    
    return mapper;
}

-(RKObjectMapping*)mappingForBusiness
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Business class]];
    [mapping addAttributeMappingsFromArray:@[@"photo_url",@"name",@"min_price",@"max_price",@"on_sale",@"service_name",@"next_appointment_times",@"yelp_rating_image_url"]];
    
    RKRelationshipMapping *relationship = [RKRelationshipMapping relationshipMappingFromKeyPath:@"location" toKeyPath:@"location" withMapping:[self mappingForLocation]];
    
    [mapping addPropertyMapping:relationship];
     
    return mapping;
}

-(RKObjectMapping*)mappingForLocation
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[Location class]];
    [mapping addAttributeMappingsFromDictionary:@{@"lon": @"longitude", @"lat": @"latitude"}];
    
    return mapping;
}

-(RKObjectMapping*)mappingForErrors
{
    RKObjectMapping *mapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
    [mapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"errorMessage"]];
    return mapping;
}

@end
