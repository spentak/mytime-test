//
//  DesignManager.m
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import "DesignManager.h"
#import "UIView+ViewFX.h"
@implementation DesignManager


+(UIColor*)getMyTimeTeal
{
    return [UIColor colorWithRed:49.0/255.0 green:159.0/255.0 blue:181.0/255.0 alpha:1.0];
}

+(UIColor*)getHeadingTextColor
{
    CGFloat color = 79.0/255.0;
    return [UIColor colorWithRed:color green:color blue:color alpha:1.0];
}

+(UIColor*)getSubHeadingTextColor
{
    CGFloat color = 113.0/255.0;
    return [UIColor colorWithRed:color green:color blue:color alpha:1.0];
}

+(UIColor*)getDividerBorderColor
{
    CGFloat color = 204.0/255.0;
    return [UIColor colorWithRed:color green:color blue:color alpha:color];
}

+(void)roundTableViewCellCorners:(UIView*)view
{
    [UIView roundCorners:view cornerRadius:7.0];
}

+(void)createButtonBorder:(UIView*)view
{
    [UIView colorBorder:view borderWidth:1.0 borderColor:[DesignManager getDividerBorderColor].CGColor];
}

+(UIFont*)getHeaderFontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"MuseoSans-900" size:fontSize];
}

+(UIFont*)getSubHeaderFontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"MuseoSans-500" size:fontSize];
}

+(UIFont*)getSubHeader2FontWithSize:(CGFloat)fontSize
{
    return [UIFont fontWithName:@"MuseoSans-100" size:fontSize];
}

@end
