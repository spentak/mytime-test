//
//  NSDateFormatter+FormatDate.m
//  jillsoffice
//
//  Created by Mark Price on 8/11/14.
//  Copyright (c) 2014 Verisage. All rights reserved.
//

#import "NSDateFormatter+FormatDate.h"

@implementation NSDateFormatter (FormatDate)


+(NSString*)getStringFromDate:(NSDate*)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"M/dd/yy"];
    
    return [df stringFromDate:date];
}

+(NSDate*)getDateFromString:(NSString*)stringDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc]init];
    [df setDateFormat:@"MM-dd-yyyy"];
    
    return [df dateFromString:stringDate];
}

+(NSString*)getFormattedDateStringRFC3339:(NSString*)rawDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [formatter setLocale:locale];
    [formatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
    
    NSDate *date = [formatter dateFromString:rawDate];
    
    return [NSDateFormatter getStringFromDate:date];
}

@end
