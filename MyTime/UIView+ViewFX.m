//
//  UIView+ViewBlur.m
//  jillsoffice
//
//  Created by Mark Price on 8/11/14.
//  Copyright (c) 2014 Verisage. All rights reserved.
//

#import "UIView+ViewFX.h"
#import <QuartzCore/QuartzCore.h>


@implementation UIView (ViewFX)

+(void)roundCorners:(UIView*)view cornerRadius:(CGFloat)cornerRadius
{
    view.layer.cornerRadius = cornerRadius;
    view.layer.masksToBounds = YES;
}

+(void)colorBorder:(UIView*)view borderWidth:(CGFloat)width borderColor:(CGColorRef)color
{
    view.layer.borderColor = color;
    view.layer.borderWidth = width;
}



@end
