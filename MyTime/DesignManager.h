//
//  DesignManager.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DesignManager : NSObject
+(UIColor*)getMyTimeTeal;
+(UIColor*)getHeadingTextColor;
+(UIColor*)getSubHeadingTextColor;
+(void)roundTableViewCellCorners:(UIView*)view;
+(void)createButtonBorder:(UIView*)view;
+(UIColor*)getDividerBorderColor;
+(UIFont*)getSubHeaderFontWithSize:(CGFloat)fontSize;
+(UIFont*)getHeaderFontWithSize:(CGFloat)fontSize;
+(UIFont*)getSubHeader2FontWithSize:(CGFloat)fontSize;
@end
