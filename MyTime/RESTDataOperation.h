//
//  DataOperation.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataOperationDelegate.h"
@interface RESTDataOperation : NSObject
@property (nonatomic,weak) id <DataOperationDelegate> delegate;

/*!
 * This method takes a search string and returns a list of Business
 * objects that match the search criteria
 * \param searchText The text to be used in the search
 */
-(void)search:(NSString*)searchText;

@end
