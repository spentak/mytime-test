//
//  Constants.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <Foundation/Foundation.h>

//URLS
extern NSString *const URL_BASE;
extern NSString *const URL_API_VERSION;
extern NSString *const URL_MOCK_SEARCH;

//FAILURE TYPES
extern NSString *const FAILURE_TYPE_NO_INTERNET;
extern NSString *const FAILURE_TYPE_NO_SEARCH_RESULTS;
extern NSString *const FAILURE_TYPE_GENERIC;


