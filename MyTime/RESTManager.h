//
//  RESTManager.h
//  MyTime
//
//  Created by Mark Price on 9/17/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RESTManager : NSObject

//RAHUL: In the future instead of only checking against the Internet
//connection this check might be parter of a larger check such as
//canPerformNetworkOperation,etc
@property (nonatomic,readonly) BOOL internetActive;
@property (nonatomic,readonly) RKObjectManager *objectManager;
+(id)instance;
-(BOOL)isClientError:(NSInteger)errorCode;
-(BOOL)isServerError:(NSInteger)errorCode;
@end
