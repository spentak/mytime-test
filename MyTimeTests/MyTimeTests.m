//
//  MyTimeTests.m
//  MyTimeTests
//
//  Created by Mark Price on 9/16/14.
//  Copyright (c) 2014 Spentak. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Business.h"
#import "Location.h"
#import "RESTRequestOperation.h"
#import "RESTObjectMapper.h"

@interface MyTimeTests : XCTestCase

@end

@implementation MyTimeTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Request Operations
-(void)testCreateGETRequestOperation
{
    NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
    RKObjectMapping *mapping = [[RKObjectMapping alloc]init];
    
    RKObjectRequestOperation *operation = [[RESTRequestOperation instance] requestOperationGETWithURL:url mapping:mapping];
    
    XCTAssertNotNil(operation, @"Request operation cannot be nil");
}

-(void)testCreatePOSTRequestOperation
{
    NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
    RKObjectMapping *mapping = [[RKObjectMapping alloc]init];
    
    RKObjectRequestOperation *operation = [[RESTRequestOperation instance] requestOperationPOSTWithURL:url mapping:mapping];
    
    XCTAssertNotNil(operation, @"Request operation cannot be nil");
}

-(void)testCreatePUTRequestOperation
{
    NSURL *url = [NSURL URLWithString:@"http://www.google.com"];
    RKObjectMapping *mapping = [[RKObjectMapping alloc]init];
    
    RKObjectRequestOperation *operation = [[RESTRequestOperation instance] requestOperationPUTWithURL:url mapping:mapping];
    
    XCTAssertNotNil(operation, @"Request operation cannot be nil");
}

#pragma mark - Model Tests
-(void)testCanCreateBusiness
{
    Business *business = [[Business alloc]init];
    XCTAssertNotNil(business, @"Business object cannot be nil");
}

-(void)testCanCreateLocation
{
    Location *location = [[Location alloc]init];
    XCTAssertNotNil(location, @"Location object cannot be nil");
}

#pragma mark - Object Mapping
-(void)testCreateBusinessMapping
{
    RKObjectMapping *mapping = [[RESTObjectMapper instance]mappingForBusiness];
    XCTAssertNotNil(mapping, @"Mapping for business cannot be nil");
}

-(void)testCreateLocationMapping
{
    RKObjectMapping *mapping = [[RESTObjectMapper instance]mappingForLocation];
    XCTAssertNotNil(mapping, @"Mapping for location cannot be nil");
}

//Some tests I would like to have written but didn't have time for
-(void)testMappingForBusinessWithMockJSON
{
    
}

-(void)testMappingForLocationWithMockJSON
{
    
}

#pragma mark - Error Handling
-(void)testIsClientErrorMin
{
    XCTAssertTrue([[RESTManager instance]isClientError:400], @"Is not client error");
}

-(void)testIsClientErrorMax
{
    XCTAssertTrue([[RESTManager instance]isClientError:499], @"Is not client error");
}

-(void)testIsClientErrorMid
{
    XCTAssertTrue([[RESTManager instance]isClientError:439], @"Is not client error");
}

-(void)testIsServerErrorMin
{
    XCTAssertTrue([[RESTManager instance]isServerError:500], @"Is not client error");
}

-(void)testIsServerErrorMax
{
    XCTAssertTrue([[RESTManager instance]isServerError:599], @"Is not client error");
}

-(void)testIsServerErrorMid
{
    XCTAssertTrue([[RESTManager instance]isServerError:539], @"Is not client error");
}

@end
